<?php
/*
 * The sidebar file for product pages
 * 
 * @package WordPress
 * @subpackage Strident
 */
?>

<aside id="sidebar">
	
  <div class="sidebar-tile t-purple contact">
    <h3>Contact Strident</h3>
    <a href="tel:01473835281" title="Call Strident">01473 835 281</a>
  </div>
  
  <?php // Get the tag for the current page
  $tags = wp_get_post_tags( $post->ID, array( 'fields' =>'ids' ) );
  $product = get_post_custom_values( 'product_tag', $post->ID );
  $promote = get_post_custom_values( 'ms_promote', $post->ID );
  ?>
  
  <div class="sidebar-tile t-orange">
    <h3>Resources</h3>
    <ul class="post-list">
    <?php
    if ( !empty( $product )) {
    $news_query = new WP_Query( 'posts_per_page=5&tag=' . $product[0] );
    if ( $news_query->have_posts() ) {
    while ( $news_query->have_posts()) : $news_query->the_post() ?>
    <li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
    <?php 
    endwhile;
    wp_reset_postdata();
    } else {
    echo "<li>There are no related news or blog articles.</li>";
    }
    } else {
    echo "<li>This page is not tagged</li>";
    } ?>
    </ul>
  </div><!-- .related-news -->
  
  <div class="sidebar-tile t-blue">
    <h3>Related Solutions</h3>
    <ul class="post-list">
    <?php
    if ( !empty( $product )) {
    $solution_query = new WP_Query( 'post_type=page&tag=' . $product[0] );
    if ( $solution_query->have_posts() ) {
    while ( $solution_query->have_posts()) : $solution_query->the_post() ?>
    <li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
    <?php 
    endwhile;
    wp_reset_postdata();
    } else {
    echo "<li>There are no related solutions.</li>";
    }
    } else {
    echo "<li>This page is not tagged</li>";
    } ?>
    </ul>
  </div><!-- .related-solutions -->
  
  <?php if ( !empty( $promote )) { 
    echo $promote[0];
  } ?>

</aside><!-- #sidebar -->