<?php
/*
 * The index file for the theme
 * 
 * @package WordPress
 * @subpackage Strident
 */
?>

<?php get_header(); ?>

<body <?php body_class(); ?>>

	<?php //Get the post
		the_post(); ?>
		
	<header id="header" role="header">
		
		<nav id="access" role="navigation">
			<div class="navigation">
				<?php wp_nav_menu(); ?>
			</div><!-- .navigation -->
			<?php get_template_part( '_contact' ); ?>
		</nav>

		<div id="branding">
			<div class="page-title">
				<h1><?php the_title(); ?></h1>
			</div><!-- .page-title-->
			<?php get_template_part( '_social' ); ?>
		</div><!-- .branding -->
		
	</header>

	<section id="content" class="fp" role="main">
			
			<a href="managed-services/" class="tile t-blue w300 ms">
				<h3 class="service-link">Help me manage my company</h3>
				<p>Strident <em>Managed Services</em> allow you to concentrate on running your business.</p>
			</a>

			<a href="business-improvement/" class="tile t-red w300 bi">
				<h3 class="service-link">Help me improve my business</h3>
				<p>Strident <em>Business Improvement</em> solutions streamline your company efficiency.</p>
			</a>
			
			<a href="cloud-computing" class="tile t-purple w300 com">
				<h3 class="service-link">Reduce risk within my workplace</h3>
				<p>Strident <em>Cloud Computing</em> take the pain away from local resources.</p>
			</a>

			<a href="talk/" class="tile t-blue live w300 talk">
			 <h3 class="service-link">Business Talk</h3>
			 <div class="inactive">
			 	<p>Have you read our latest copy of Business Talk and want to know how Strident can help?</p>
			 </div>
			 <div class="active">
			   <img src="<?php echo IMAGES; ?>/talk.png" />
			 </div>
			</a>
			
			<a href="products/" class="tile t-green w300 pro">
				<h3 class="service-link">Products and Services</h3>
				<p>View all of the products and service's Strident has to offer</p>
			</a>
			
			<a href="resources/" class="tile live t-red w300 resources">
			  <h3 class="service-link">Resources</h3>
			  <div class="inactive">
  				<p>The resources page</p>
  			</div>
  			<div class="active">
  			  <img src="<?php echo IMAGES; ?>/wallpaper.png" />
  			</div>
			</a>
			
			<a href="news/" class="tile live t-orange w300 news">
				<h3 class="service-link">News and Views</h3>
				<div class="inactive">
				  <p>Keep up to date with all the news and views from Strident.</p>
				</div>
				<div class="active">
  				<ul class="post-list">
  				  <?php
  				    $args = array( 'numberposts' => 3, 'post_type' => 'post' );
  				    $posts_array = get_posts( $args );
  				    if ( !empty( $posts_array ) ) {
  				      foreach ($posts_array as $post) : setup_postdata( $post ); ?>
  				        <li><?php the_title(); ?></li>
  				      <?php endforeach;
  				    } else {
  				      echo "<li>No posts to report.</li>";
  				    }
  				    wp_reset_query();
  				  ?>
  				</ul>
  			</div>
			</a>
			
			<a href="about/" class="tile t-blue w300 about">
				<h3 class="service-link">About Strident</h3>
				<?php the_content(); ?>
			</a>

			<a href="#" class="tile t-purple live w300 case-study">
			  <h3 class="service-link">Featured Case Study</h3>
			  <div class="inactive">
			  	<p>Read all about this featured case study.</p>
			  </div>
			  <div class="active">
			    <img src="<?php echo IMAGES; ?>/case-study.png" />
			  </div>
			</a>
		
	</section><!-- #content -->
	
	<script>
	
	    jQuery(document).ready(function($){
	
	      setupTiles();
	
	      setInterval(function() {
	        setupTiles();
	      }, 8000);
	    });
	
	    function setupTiles()
	    {
	      var tiles = jQuery('.live[data-active!="active"] > div.active').each(function() {
	        activateTile(this);
	      });
	    }
	 
	    function activateTile(tile) {
	      var intro = getRandomArbitrary(5000, 10000);
	      var pause = getRandomArbitrary(500,1000);
	      var outro = getRandomArbitrary(2000, 5000);
	
	      //var elm = jQuery(tile);
	      //var parentElement = jQuery(elm).parent();
	        
	      setTimeout(function(){
	         jQuery(tile).animate({ top: 50}, 500);
	         jQuery(tile).parent().attr('data-active','active');  
	      }, intro);
	
	      setTimeout(function() {
	        jQuery(tile).animate({ top: 168 }, 500);
	        jQuery(tile).parent().removeAttr('data-active');
	      }, intro + pause + outro);
	    }
	
	    function getRandomArbitrary(min, max) {
	      return Math.random() * (max - min) + min;
	    }
	
	  </script>

<?php get_footer(); ?>