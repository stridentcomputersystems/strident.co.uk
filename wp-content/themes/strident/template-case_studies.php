<?php
/*
 * Template Name: Case Study Group Page
 * 
 * @package WordPress
 * @subpackage Strident
 */
?>

<?php get_header(); ?>

<body <?php body_class(); ?>>

	<?php //Get the post
		the_post(); ?>
		
	<header id="header" role="header">
		
		<nav id="access" role="navigation">
			<div class="navigation">
				<?php wp_nav_menu(); ?>
			</div><!-- .navigation -->
			<?php get_template_part( '_contact' ); ?>
		</nav>

		<div id="branding">
			<div class="page-title">
				<h1><?php the_title(); ?></h1>
			</div><!-- .page-title-->
			<?php get_template_part( '_social' ); ?>
		</div><!-- .branding -->
		
	</header>

<section id="content">
		
	<section id="main" class="single-page" role="main">
		
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<header class="entry-header purple">
					<h1 class="entry-title">
						<?php the_title(); ?>
					</h1>
				</header><!-- .entry-header -->
				<div class="entry-content">
					<?php the_content(); ?>
				</div><!-- .entry-content -->
			</article><!-- #post-<?php the_ID(); ?> -->

			<?php // Start the loop
			$cs_query = new WP_Query( 'post_type=case_study');

			while ( $cs_query->have_posts()) : $cs_query->the_post() ?>

			<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark" id="post-<?php the_ID(); ?>" class="tile case-study-tile">
				<header class="entry-header">
					<h3 class="entry-title"><?php the_title(); ?></h3>
				</header><!-- .entry-header -->
				<section class="entry-summary">
					<?php if( has_post_thumbnail()) {
						the_post_thumbnail( 'business-talk-thumb' );
					
						}
						the_excerpt();
					?>
				</section><!-- .entry-summary -->
			</a><!-- .post -->

		<?php 
		endwhile; ?>

	</section><!-- #main -->

	<?php get_sidebar(); ?>
	
</section><!-- #content -->

<?php get_footer(); ?>