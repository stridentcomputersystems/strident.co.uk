<?php
/*
 * Template Name: Contact Page Page
 * 
 * This template is used for the BContact Strident main page
 *
 */
?>

<?php get_header(); ?>

<body <?php body_class(); ?>>

	<?php //Get the post
		the_post(); ?>
		
	<header id="header" role="header">
		
		<nav id="access" role="navigation">
			<div class="navigation">
				<?php wp_nav_menu(); ?>
			</div><!-- .navigation -->
			<?php get_template_part( '_contact' ); ?>
		</nav>

		<div id="branding">
			<div class="page-title">
				<h1><?php the_title(); ?></h1>
			</div><!-- .page-title-->
			<?php get_template_part( '_social' ); ?>
		</div><!-- .branding -->
		
	</header>

<section id="content">
		
	<section id="main" class="single-page" role="main">
		
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<header class="entry-header green">
					<h1 class="entry-title">
						<?php the_title(); ?>
					</h1>
				</header><!-- .entry-header -->
				<div class="entry-content">
					<?php the_content(); ?>
				</div><!-- .entry-content -->
			</article><!-- #post-<?php the_ID(); ?> -->

	</section><!-- #main -->

  <?php include (TEMPLATEPATH . '/sidebar-contact.php'); ?>
	
</section><!-- #content -->

<?php get_footer(); ?>