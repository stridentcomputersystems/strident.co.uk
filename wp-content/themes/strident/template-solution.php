<?php
/*
 * Template Name: Solution Page
 * 
 * This template is used for the Solution page
 *
 */
?>

<?php get_header(); ?>

<body <?php body_class(); ?>>

	<?php //Get the post
		the_post(); ?>
		
	<header id="header" role="header">
		
		<nav id="access" role="navigation">
			<div class="navigation">
				<?php wp_nav_menu(); ?>
			</div><!-- .navigation -->
			<?php get_template_part( '_contact' ); ?>
		</nav>

		<div id="branding">
			<div class="page-title">
				<h1><?php the_title(); ?></h1>
			</div><!-- .page-title-->
			<?php get_template_part( '_social' ); ?>
		</div><!-- .branding -->
		
	</header>

<section id="content">

	<?php if( $post->post_parent ) {
		$parent_link = get_permalink( $post->post_parent );
		$parent_title = get_the_title( $post->post_parent ); ?>
		<a href="<?php echo $parent_link; ?>" class="up-link" title="Up to <?php echo $parent_title; ?>">Up to <?php echo $parent_title; ?></a>
	<?php } ?>
		
	<section id="main" role="main">
		
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<?php
					$colour = get_post_meta( $post->ID, 'tile_colour', true);
				?>
				<header class="entry-header<?php if (!empty( $colour )) { echo " " . $colour; } ?>">
					<h1 class="entry-title">
						<?php the_title(); ?>
					</h1>
				</header><!-- .entry-header -->
				<div class="entry-content">
					<?php the_content(); ?>
				</div><!-- .entry-content -->
			</article><!-- #post-<?php the_ID(); ?> -->

	</section><!-- #main -->

  <?php include (TEMPLATEPATH . '/sidebar-solution.php'); ?>
  
</section><!-- #content -->

<?php get_footer(); ?>