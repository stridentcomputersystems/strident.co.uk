<?php
/**
 * Template Name: White Paper Group Page
 *
 * This template is used for the White Paper selector page
 *
 */

get_header(); ?>

<body <?php body_class(); ?>>

	<?php //Get the post
		the_post(); ?>
		
	<header id="header" role="header">
		
		<nav id="access" role="navigation">
			<div class="navigation">
				<?php wp_nav_menu(); ?>
			</div><!-- .navigation -->
			<?php get_template_part( '_contact' ); ?>
		</nav>

		<div id="branding">
			<div class="page-title">
				<h1><?php the_title(); ?></h1>
			</div><!-- .page-title-->
			<?php get_template_part( '_social' ); ?>
		</div><!-- .branding -->
		
	</header>

	<section id="content" class="product">
		<?php
			global $post;
			$args = array(
						'numberposts' => 100,
						'post_type' => 'white_paper'
					);
			$posts_array = get_posts( $args );
			?>
			<pre>
			 <?php print_r( $posts_array ); ?>
			</pre>
			<?php foreach( $posts_array as $post) : setup_postdata( $post );
			$tags = the_tags();
			echo print_r( $tags );
			endforeach; ?>
	</section>

<?php get_footer(); ?>