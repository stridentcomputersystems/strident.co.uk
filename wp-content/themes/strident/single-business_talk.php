<?php
/*
 * The single post page for the theme
 * 
 * @package WordPress
 * @subpackage Strident
 */
?>

<?php get_header(); ?>

<body <?php body_class(); ?>>

  <?php the_post(); ?>
    
  <header id="header" role="header">
    
    <nav id="access" role="navigation">
      <div class="navigation">
        <?php wp_nav_menu(); ?>
      </div><!-- .navigation -->
      <?php get_template_part( '_contact' ); ?>
    </nav>

    <div id="branding">
      <div class="page-title">
        <h1><?php the_title(); ?></h1>
      </div><!-- .page-title-->
      <?php get_template_part( '_social' ); ?>
    </div><!-- .branding -->
    
  </header>

  <section id="content">

    <section id="main" class="single-post" role="main">

      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
          <?php
            $colours = array( 1 => 'blue', 2 => 'green', 3 => 'orange', 5 => 'purple', 6 => 'red');
            $colour = $colours[ rand( 1, 6 ) ];
          ?>
        <header class="entry-header <?php echo $colour; ?>">
          <h1 class="entry-title">
            <?php the_title(); ?>
          </h1>
        </header><!-- .entry-header -->
        <section class="entry-content">
          <?php the_content(); ?>
        </section><!-- .entry-summary -->
      </article><!-- .post -->

      <?php //comments_template('', true); //Load the comments ?>

    </section><!-- #main -->

    <aside id="sidebar">
      <div class="sidebar-tile t-blue">
        <h3>Volume Quick Links</h3>
        <?php 
          $links = get_post_meta( $post->ID, 'bt_quicklinks', true);
          if (!empty( $links )) {
            echo '<ul class="post-list">';
            $counter = 1;
            foreach(preg_split("/(\r?\n)/", $links) as $link){
              echo "<li><a href='#{$counter}''>{$link}</a></li>";
              $counter++;
            }
            echo "</ul>";
          } else {
            echo "There are no defined quick links";
          }
        ?>
        </ul>
      </div>
      <div class="sidebar-tile t-green">
        <h3>Volume Cover</h3>
        <?php the_post_thumbnail(); ?>
      </div>
      <div class="sidebar-tile t-purple">
        <h3>Get your copy</h3>
        <?php echo do_shortcode( '[contact-form-7 id="393" title="BusinessTalk contact form"]' ); ?>
      </div>
    </aside>
    
  </section><!-- #content -->

<?php get_footer(); ?>