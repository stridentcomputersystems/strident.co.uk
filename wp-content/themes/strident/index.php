<?php
/*
 * The index file for the theme
 * 
 * @package WordPress
 * @subpackage Strident
 */
?>

<?php get_header(); ?>

<body <?php body_class(); ?>>
		
	<header id="header" role="header">
		
		<nav id="access" role="navigation">
			<div class="navigation">
				<?php wp_nav_menu(); ?>
			</div><!-- .navigation -->
			<?php get_template_part( '_contact' ); ?>
		</nav>

		<div id="branding">
			<div class="page-title">
				<h1><?php the_title(); ?></h1>
			</div><!-- .page-title-->
			<?php get_template_part( '_social' ); ?>
		</div><!-- .branding -->
		
	</header>

	<section id="content">

		<section id="main" class="home-post" role="main">
		
			<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<header class="entry-header">
							<aside class="entry-meta">
								<time datetime="<?php the_time( 'Y-m-d' ); ?>"><span class="date-day"><?php the_time( 'j' ); ?></span> <span class="date-month"><?php the_time( 'F' ); ?></time>
							</aside><!-- .entry-meta -->
							<h3 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark"><?php the_title(); ?></a></h3>
						</header><!-- .entry-header -->
						<section class="entry-summary">
							<?php the_excerpt(); ?>
						</section><!-- .entry-summary -->
						<footer class="entry-footer">
							<a class="more-link" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark">Continue reading</a>
						</footer><!-- entry-footer -->
					</article><!-- .post -->
				<?php endwhile; ?>
				<?php else : ?>
					<div id="post-0" <?php post_class(); ?>>
						<div class="entry-content">
							<p>The content you were looking for could not be found.</p>
						</div><!-- .entry-content -->
					</div><!-- .post -->
			<?php endif; ?>

			<?php get_template_part('_navigation'); ?>

		</section><!-- #main -->

		<?php get_sidebar(); ?>
		
	</section><!-- #content -->

<?php get_footer(); ?>