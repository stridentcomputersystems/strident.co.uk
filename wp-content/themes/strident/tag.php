<?php
/*
 * The tag page for the theme
 * 
 * @package WordPress
 * @subpackage Strident
 */
?>

<?php get_header(); ?>

<body <?php body_class(); ?>>

	<?php the_post(); ?>
		
	<header id="header" role="header">
		
		<nav id="access" role="navigation">
			<div class="navigation">
				<?php wp_nav_menu(); ?>
			</div><!-- .navigation -->
			<?php get_template_part( '_contact' ); ?>
		</nav>

		<div id="branding">
			<div class="page-title">
				<h1>Tagged with <?php single_tag_title(); ?></h1>
			</div><!-- .page-title-->
			<?php get_template_part( '_social' ); ?>
		</div><!-- .branding -->
		
	</header>

	<section id="content">

		<section id="main" class="archive-post" role="main">

			<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>

			<header class="entry-header purple">
				<h1 class="entry-title">
					Content tagged with <?php single_tag_title(); ?>
				</h1>
			</header><!-- .entry-header -->

			<?php $product_query = new WP_Query( 'post_type=page&posts_per_page=50&tag=' . get_query_var('tag') ); ?>
			<div class="tile tagged-solutions t-blue w220">
				<h3>Products and Solutions</h3>
				<ul class="post-list">
				<?php
				if ( $product_query->have_posts() ) { 
					while ( $product_query->have_posts()) : $product_query->the_post() ?>
						<li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
					<?php 
					endwhile;
					wp_reset_postdata();
				} else {
					echo "<li>There are no related case studies.</li>";
				} ?>
				</ul>
			</div>
			
			<h2>Articles associated with <?php single_tag_title(); ?></h2>
				<?php
				$query = new WP_Query( 'post_type=post&posts_per_page=50&tag=' . get_query_var('tag') . '&paged=' . $paged );
				$bgcolour = "t-purple"; //( is_category('Blog')) ? "t-blue" : "t-purple";
				if ( $query->have_posts() ) { 
					while ( $query->have_posts()) : $query->the_post() ?>
						<?php $bgcolour = ( in_category( 'News' ) ) ? "t-green" : "t-orange"; ?>
						<a href="<?php the_permalink(); ?>" id="post-<?php the_ID(); ?>" <?php post_class("tile w280 {$bgcolour}"); ?>>
							<h3 class="service-link"><?php the_title(); ?></h3>
							<time datetime="<?php the_time( 'Y-m-d' ); ?>">Posted on <?php the_time( 'j' ); ?> <?php the_time( 'F' ); ?> <?php the_time( 'Y' ); ?></time>
						</a><!-- .post-<?php the_ID(); ?> -->
					<?php 
					endwhile;
					wp_reset_postdata();
				} else {
					echo "<p>There are no articles currently stored.</p>";
				} ?>

			<?php get_template_part('_navigation'); ?>

		</section><!-- #main -->

		<?php get_sidebar(); ?>
		
	</section><!-- #content -->

<?php get_footer(); ?>