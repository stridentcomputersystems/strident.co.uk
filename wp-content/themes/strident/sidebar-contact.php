<?php
/*
 * The sidebar file for contact pages
 * 
 * @package WordPress
 * @subpackage Strident
 */
?>

<aside id="sidebar">
	<div class="sidebar-tile t-green">
		<h3>Claydon Office</h3>
		<address>
			Strident Computer Systems Ltd.<br />
			Claydon Court<br />
			Old Ipswich Road<br />
			Claydon<br />
			Suffolk<br />
			IP6 0AE
		</address>
	</div>
	<div class="sidebar-tile t-orange">
		<h3>Ipswich Office</h3>
		<address>
			Strident Computer Systems Ltd.<br />
			Crown House<br />
			Crown Street<br />
			Ipswich<br />
			Suffolk<br />
			IP1 3HS
		</address>
	</div>
</aside>