<?php
/*
 * The functions file for the theme
 * 
 * @package WordPress
 * @subpackage Strident
 */

/*
 * Define the default temppath and images path
 */
define('TEMP_PATH', get_bloginfo('stylesheet_directory'));
define('IMAGES', TEMP_PATH . "/images");
 
/*
 * Get the page number
 */
function get_page_number() {
	if(get_query_var('paged')) {
		print ' | ' . __('Page ', 'strident') . get_query_var('paged');
	}
}

/*
 * Register Navigation Menus
 */
if(function_exists('register_nav_menus')) {
	register_nav_menus(
		array(
			'main' => 'Main Navigation',
			'footer' => 'Footer Navigation'
		)
	);
}

/*
 * Register widgetised areas
 */
function theme_widgets_init() {
	register_sidebar(array(
		'name' => 'Posts Sidebar',
		'id' => 'post-sidebar',
		'before_widget' => '<div class="sidebar-tile %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	));
}

/*
 * Custom callback to list comments
 */
function custom_comments($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment;
	$GLOBALS['comment_depth'] = $depth;
	?>
	<li id="comment-<?php comment_ID(); ?>" <?php comment_class(); ?>>
		<div class="comment-author vcard">
			<?php commenter_link(); ?>
		</div><!-- .comment-author -->
		<div class="comment-meta">
			<?php printf(__('Posted %1$s at %2$s <span class="meta-sep"> | </span> <a href="%3$s" title="Permalink to this comment">Permalink</a>', 'inception'),
				get_comment_date(),
				get_comment_time(),
				'#comment-' . get_comment_ID());
				edit_comment_link(__('Edit', 'inception'), ' <span class="meta-sep"> | </span> <span class="edit-link">', '</span>'); ?>
		</div><!-- .comment-meta -->
		<?php if($comment->comment_approved == '0') _e("<span class\"unapproved\">Your comment is awaiting moderation.</span>\n", 'inception'); ?>
		<div class="comment-content">
			<?php comment_text(); ?>
		</div><!-- .comment-content -->
		<?php if($args['type'] == 'all' || get_comment_type() == 'comment') :
			comment_reply_link(array_merge($args, array(
				'reply_text' => __('Reply', 'inception'),
				'login_text' => __('Log in to reply', 'inception'),
				'depth' => $depth,
				'before' => '<div class="comment-reply-link">',
				'after' => '</div>'
			)));
		endif; ?>
	</li>
<?php }

/*
 * Custom callback to list pings
 */
function custom_pings($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment;
	?>
	<li id="comment-<?php comment_ID(); ?>" <?php comment_class(); ?>>
		<div class="comment-author">
			<?php printf(__('By %1$s on %2$s at %3$s', 'inception'),
				get_comment_author_link(),
				get_comment_date(),
				get_comment_time() );
				edit_comment_link(__('Edit', 'inception'), '<span class="meta-sep"> | </span><span class="edit-link">', '</span>'); ?>
		</div><!-- .comment-author -->
		<?php if($comment->comment_approved == '0') _e("<span class=\"unapproved\">Your trackback is awaiting moderation.</span>\n", 'inception'); ?>
		<div class="comment-content">
			<?php comment_text(); ?>
		</div><!-- .comment-content -->
	</li>
<?php }

/*
 * Produces an avatar
 */
function commenter_link() {
	$commenter = get_comment_author_link();
	if(ereg('<a[^>]* class=[^>]+>', $commenter)) {
		$commenter = ereg_replace('(<a[^>]* class=[\'"]?)', '\\1url ', $commenter);
	} else {
		$commenter = ereg_replace('(<a )/', '\\1class="url "', $commenter);
	}
	$avatar_email = get_comment_author_email();
	$avatar = str_replace("class='avatar'", "class='photo avatar", get_avatar($avatar_email, 80));
	echo $avatar . '<span class="fn n"' . $commenter . '</span>';
}

/*
 * Create Case Studies custom post type
 */
function case_study_post_type() {
	register_post_type( 'case_study',
		array(
			'labels' => array(
				'name' => __( 'Case Studies' ),
				'singular_name' => __( 'Case Study' )
				),
			'public' => true,
			'menu_position' => 6,
			'rewrite' => array( 'slug', 'case-study'),
			'supports' => array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'custom-fields',
				'page-attributes'
				)
		)
	);
}

/*
 * Create Business Talk custom post type
 */
function business_talk_post_type() {
	register_post_type( 'business_talk',
		array(
			'labels' => array(
				'name' => __( 'Business Talk'),
				'singular_name' => __( 'Business Talk' ),
				'edit_item' => __( 'Edit Edition' )
				),
			'public' => true,
			'menu_position' => 5,
			'rewrite' => array( 'slug', 'business-talk' ),
			'hierarchical' => true,
			'supports' => array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'custom-fields',
				'page-attributes'
				)
		)
	);
}

/*
 * Add tags for the page and case studies content types
 */
function tags_for_pages_and_custom_post_types() {
	register_taxonomy_for_object_type( 'post_tag', 'page' );
	register_taxonomy_for_object_type( 'post_tag', 'case_study' );
}

/*
 * Add theme support and functions
 */
add_theme_support( 'admin-bar', array( 'callback' => '__return_false') );
add_theme_support( 'nav_menus' );
add_theme_support( 'post-thumbnails' ); 
add_action( 'init', 'case_study_post_type' );
add_action( 'init', 'business_talk_post_type' );
add_action( 'init', 'theme_widgets_init' );
add_action( 'admin_init', 'tags_for_pages_and_custom_post_types' );
add_image_size( 'business-talk-thumb', 100, 9999 );